//
//  ViewController.swift
//  Sad Trombone
//
//  Created by manuel on 03/11/2022.
//

import UIKit
import AVFoundation

class ViewController: UIViewController {

    var audioPlayer : AVAudioPlayer?
    
    @IBAction func sadFacePressed(_ sender: UIButton) {
        do {
            let fileURL = Bundle.main.path(forResource: "sad_trombone.wav", ofType: nil)!
            audioPlayer = try AVAudioPlayer(contentsOf: URL(fileURLWithPath: fileURL))
            audioPlayer!.prepareToPlay()
            audioPlayer!.play()
        } catch let error {
            fatalError("Can't play the audio file failed with an error \(error.localizedDescription)")
        }
    }
    

}

